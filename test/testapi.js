const mocha = require('mocha');
const chai = require('chai');
const chaihttp = require('chai-http');

chai.use(chaihttp);

var should = chai.should();
var server =require('../server');

describe('First test',
  function(){
    it('Test that Duckduckgo works', function(done) {
        chai.request('http://www.duckduckgo.com')
          //el . me permite encadenar metodos
          .get('/')
          //la parte manejadora
          .end(
            function(err, res) {
              console.log("Request finished");
              console.log(err);
              //console.log(res);

              res.should.have.status(200);
              //este done es una especie de return de esta parte
              done();
            }
          )
      }
    )

  }
)

describe('Test de API de Usuarios',
  function(){
    it('Prueba que la API de usuarios responde', function(done) {
        chai.request('http://localhost:3000')
          //el . me permite encadenar metodos
          .get('/apitechu/v1/hello')
          //la parte manejadora
          .end(
            function(err, res) {
              console.log("Request finished");
              console.log(err);
              //console.log(res);
              res.should.have.status(200);
              res.body.msg.should.be.eql("Hola desde API TechU!");
              //este done es una especie de return de esta parte
              done();
            }
          )
      }
    ),
    it('Prueba que la API devuelve la lista de usuarios correcta', function(done) {
        chai.request('http://localhost:3000')
          .get('/apitechu/v1/users')
          .end(
            function(err, res) {
              console.log("Request finished");
              //compruebo que el resultado es 200 para empezar
              res.should.have.status(200);
              //res.body.msg.should.be.eql("Request finished");

              //añado validaciṕn de que todos los usuarios tienen un formato correcto, he puesto que como minimo
              //tengan email y password
              for (user of res.body.users) {
                user.should.have.property("email");
                user.should.have.property("password");
              }

              done();
            }
          )
      }
    )
  }
)
