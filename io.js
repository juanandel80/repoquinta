    const fs = require('fs');

    function writeUserDataToFile (data) {
      var jsonUserData = JSON.stringify(data);

      fs.writeFile("./usuariospass.json", jsonUserData, "utf8",
        function(err) {
          if (err) {
            console.log(err);
          } else {
            console.log("Datos escritos en el fichero");
          }
        }
      );

    }

//hago esto para tener disponible esta función en otros módulos
    module.exports.writeUserDataToFile = writeUserDataToFile;
