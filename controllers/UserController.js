const io = require('../io');
const crypt = require('../crypt');

//defino las nuevas constantes para los request de MongoDB con APIs
const requestJson = require('request-json');
const mlabBaseURL = "https://api.mlab.com/api/1/databases/apitechu8edjapp/collections/";
const mlabAPIKey = "apiKey=" + process.env.MLAB_API_KEY;

function getUsersV1 (req, res){
  console.log("GET /apitechu/v1/users");
  console.log(req.query);
  console.log(req.query.$count);
  console.log(req.query.$top);

//indicamos ../usuarios.json porque ahora está dos capas más atrás
  var users = require('../usuariospass.json');
  var result = {};

  if (req.query.$count == "true") {
    console.log("req.query.&count&$top");
    result.count = users.length;
  };

  //otra forma de hacer el If   (true:false)
  // ob.userrs = req.query.&top ? users-slice(0, req.query.&top) : userrs;
  //res.send(ob)

  if (req.query.$top) {
    console.log("req.query$top");
    result.users = users.slice(0,req.query.$top);
    res.send(result);
  }
  else {
    console.log("todos los usuarios");
    result.users = users;
    res.send(result);
  };
}

//FUNCIÓN GET USER accediento a MongoDB

function getUsersV2 (req, res) {
  console.log("Entramos en GET /apitechu/v2/users");

//estamos creando un cliente httpClient, inicializado a una URL mlabBaseURL
  var httpClient = requestJson.createClient(mlabBaseURL);
  console.log("Client created");

//especificamos el método de la petición.get  y definimos función con objeto Error y resMLab completo y body con respuesta del servidor
  httpClient.get("user?" + mlabAPIKey,
    function(err, resMLab, body) {
//con esto response recoge la respuesta body si va OK
      var response = !err ?
        body : {"msg" : "Error obteniendo usuarios"}

        res.send(response);
    }
  )
}

//Vamos a crear una consulta con "id" de usuario:

function getUserByIdV2 (req, res) {
  console.log("Entramos en GET /apitechu/v2/users/:id");

  var id = req.params.id;
  var query = 'q={"id":' + id + '}';

  console.log(query);

  var httpClient = requestJson.createClient(mlabBaseURL);

  console.log("user?" + query + '&' + mlabAPIKey);

  httpClient.get("user?" + query + '&' + mlabAPIKey,
    function(err, resMLab, body) {

      if (err) {

        var response = {
            "msg" : "Error obteniendo usuario"
        };
        res.status(500);
      } else {
        if (body.length > 0) {
          var response = body[0];
        } else {
          var response = {
            "msg" : "Usuario no encontrado"
          };
          res.status(404);
        }
      }
//      var response = !err ?
//        body : {"msg" : "Error obteniendo usuarios"}
//
        res.send(response);

    }
  )
}

//Alta de usuario sobre fichero

function createUsersV1(req, res){
      console.log("POST /apitechu/v1/users");

      console.log(req.body.first_name);
      console.log(req.body.last_name);
      console.log(req.body.email);
      console.log(req.body.password);


      var newUser = {
        "first_name" : req.body.first_name,
        "last_name" : req.body.last_name,
        "email" : req.body.email,
        "password" : req.body.password
      };

      var users = require ("../usuariospass.json");
      users.push(newUser);

//usaremos una función que definimos más abajo
      io.writeUserDataToFile(users);

      res.send("Usuario añadido con éxito");
    }

//FUNCION alta de usuario a partir contra MongoDB

    function createUsersV2(req, res){
          console.log("Entrando en POST /apitechu/v2/users");

          console.log(req.body.id);
          console.log(req.body.first_name);
          console.log(req.body.last_name);
          console.log(req.body.email);
          console.log(req.body.password);


          var newUser = {
            "id" : req.body.id,
            "first_name" : req.body.first_name,
            "last_name" : req.body.last_name,
            "email" : req.body.email,
            "password" : crypt.hash(req.body.password)
            //"password" : req.body.password
          };

          var httpClient = requestJson.createClient(mlabBaseURL);
          console.log("cliente creado");


          httpClient.post("user?" + mlabAPIKey, newUser,
            function(err, resMLab, body) {

              console.log("Usuario guardado con exito");
              res.status(201);
              res.send({"msg" : "Usuario creado con extito"});

            }
          )

        }

//FUNCION borrar usuario en fichero

function deleteUserV1(req, res){

        console.log("DELETE /apitechu/v1/users/:id");

        console.log("La id enviada es: " + req.params.id);

        var users = require("../usuariospass.json");
        var deleted = false;

        //Posicionarse en el usuario id y borrarlo
        console.log("bucle FOR:"+users.length);

      //Algoritmo1 borrado - con FOR

      for (var i = 0; i < users.length; i++) {

      console.log(i);
      console.log(users[i].id);

        if (users[i].id == req.params.id){
          console.log("Usuario borrado:");
          console.log(users[i]);
          users.splice(i,1);
          deleted = true;

          break;
        }
      }
          //Algoritmo1 borrado - con FOR IN - NO RECOMENDADO PARA RECORRER ARRAYS
      for (arrayId in users) {

                  if (users[arrayId].id == req.params.id){
                    console.log("Usuario borrado:");
                    console.log(users[i]);
                    users.splice(i,1);
                    deleted = true;

                    break;
                  }
                }

          //Algoritmo1 borrado - con FOR OF 1
          //Algoritmo1 borrado - con FOR OF 2




                if (deleted) {
                    io.writeUserDataToFile(users);
                }

    //función splice: coge el elemento anterior a id y borra el siguiente
    //      users.splice(req.params.id - 1,1);
    //users.splice(i - 1,1);




    //      io.writeUserDataToFile(users);
    //      console.log("Usuario borrado");
    }



module.exports.getUsersV1 = getUsersV1;
module.exports.createUsersV1 = createUsersV1;
module.exports.deleteUserV1 = deleteUserV1;
module.exports.getUsersV2 = getUsersV2;
module.exports.getUserByIdV2 = getUserByIdV2;
module.exports.createUsersV2 = createUsersV2;
