const io = require('../io');
const crypt = require('../crypt');

//defino las nuevas constantes para los request de MongoDB con APIs
const requestJson = require('request-json');
const mlabBaseURL = "https://api.mlab.com/api/1/databases/apitechu8edjapp/collections/";
const mlabAPIKey = "apiKey=" + process.env.MLAB_API_KEY;



function loginV1(req,res){
  console.log("POST /apitechu/v1/login")
  console.log("Body:");
  console.log(req.body);
  console.log(req.body.email);
  console.log(req.body.password);

  //tengo que buscar req.email en el fichero

  var encontrado = false;
  var users = require("../usuariospass.json");

//   and es "&&" y OR es "||"
  for (var i = 0; i < users.length; i++) {

    if ((users[i].email == req.body.email) && (req.body.password == users[i].password)) {
      console.log("encontrado");
      encontrado = true;
      users[i].logged = true;
      io.writeUserDataToFile(users);
      var salida = users[i].id;
      break;
    }
  }



  if (encontrado){
  //  res.send("Usuario logado con éxito");
      salida = {"mensaje" : "Login correcto" , "idUsuario" : salida};
      res.send(salida);

  }
  else {
      res.send({"mensaje" : "Error usuario o password"});
  }



}

//logon V2 - con checkpassword
function loginV2(req,res){
  console.log("POST /apitechu/v2/login")
  console.log("Body:");
  console.log(req.body);
  console.log(req.body.email);
  console.log(req.body.password);



  //tengo que buscar req.email en el fichero

  var encontrado = false;

  var query = 'q={"email":"' + req.body.email + '"}';
  console.log("La query es:" + query);

  httpClient = requestJson.createClient(mlabBaseURL);
  httpClient.get("user?" + query + '&' + mlabAPIKey,
    function(err, resMLab, body) {

      if (err) {

        var response = {
            "msg" : "Error obteniendo usuario"
        };
        res.status(500);
      } else {
        if (body.length > 0) {
          console.log(body);
          console.log("-----------");

          var userHashPassword = body[0].password;

          console.log(userHashPassword);
          //valido si password OK

          var resultado = crypt.checkpassword(req.body.password,userHashPassword)

          if (resultado===true){

            var putBody = '{"$set":{"logged":true}}';
            console.log(putBody);
          //el JSON.parse se pone para que reconozca putBody y no lo trate como cadena de caracteres
            httpClient.put("user?" + query + '&' + mlabAPIKey, JSON.parse(putBody),
              function(err, resMLabPUT, bodyPUT) {
                console.log("PUTdone");
                var response = {
                    "msg" : "Usuario logado",
                    "idUsuario" : body[0].id
                };
                res.status(200);
                console.log("enviar response1");
                console.log(response);
                res.send(response);

              }
            )

          }else{
            var response = {
                "msg" : "Error Password"
            };
            res.status(401);  //Unathorized
            res.send(response);
          }



        } else {
            var response = {
              "msg" : "Usuario no encontrado"
            };
            res.status(401); //Unathorized
            res.send(response);
        }
      }
      // console.log("enviar response2");
      // console.log(response);
      // res.send(response);

    }
  )
}






function logoutV1(req,res){
  console.log("POST /apitechu/v1/logout/")
  console.log("Body:");
  console.log(req.body);
  console.log(req.body.id);


  //tengo que buscar req.email en el fichero

  var encontrado = false;
  var users = require("../usuariospass.json");


  for (var i = 0; i < users.length; i++) {

    if ((users[i].id == req.body.id) && (users[i].logged == true)) {
      console.log("encontrado");
      encontrado = true;
      delete users[i].logged;
      io.writeUserDataToFile(users);
      var salida = users[i].id;
      break;
    }
  }

    if (encontrado){
      salida = {"mensaje" : "Logout correcto" , "idUsuario" : salida};
      res.send(salida);
    }
    else {
      res.send({"mensaje" : "Error usuario o password"});
    }



}


//logout v2

function logoutV2(req,res){
  console.log("POST /apitechu/v2/logout/:id")
  console.log("params:");
  console.log(req.params.id);
  var idsalida = req.params.id;


  var encontrado = false;

  var query = 'q={"id":' + req.params.id + '}';

  console.log("mi query es:" + query);

  //estamos creando un cliente httpClient, inicializado a una URL mlabBaseURL
  httpClient = requestJson.createClient(mlabBaseURL);
  httpClient.get("user?" + query + '&' + mlabAPIKey,
    function(err, resMLab, body) {
      console.log("entra1");
      if (err) {
        var response = {
            "msg" : "Error obteniendo usuario"
        };
        res.status(500);
      } else {
        if (body.length > 0) {

          //la función unset es para borrar
          var putBody = '{"$unset":{"logged":""}}';
          console.log(putBody);
          console.log(body[0]);
          httpClient.put("user?" + query + '&' + mlabAPIKey, JSON.parse(putBody),
            function(err, resMLab, body) {
              console.log("punto1");
              console.log(idsalida);
              var salida = {"mensaje" : "Logout correcto" , "idUsuario" : idsalida};
              res.send(salida);
            }
          )
        } else {
            res.send({"mensaje" : "Error usuario o password"});
            res.status(404);
        }
    }
  }
  )
}

module.exports.loginV1 = loginV1;
module.exports.logoutV1 = logoutV1;
module.exports.loginV2 = loginV2;
module.exports.logoutV2 = logoutV2;
