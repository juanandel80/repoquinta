const io = require('../io');
const crypt = require('../crypt');

//defino las nuevas constantes para los request de MongoDB con APIs
const requestJson = require('request-json');
const mlabBaseURL = "https://api.mlab.com/api/1/databases/apitechu8edjapp/collections/";
const mlabAPIKey = "apiKey=" + process.env.MLAB_API_KEY;

//REcuperar las cuentas del usuario userId

function getAccountByUserIdV2 (req, res) {
  console.log("Entramos en GET /apitechu/v2/accounts/:userid");
  console.log(req.params);

  var userId = req.params.id;
  var query = 'q={"userId":' + userId + '}';

  console.log(query);

  var httpClient = requestJson.createClient(mlabBaseURL);

  console.log("accounts?" + query + '&' + mlabAPIKey);

  httpClient.get("accounts?" + query + '&' + mlabAPIKey,
    function(err, resMLab, body) {

      if (err) {

        var response = {
            "msg" : "Error obteniendo cuentas usuario"
        };
        res.status(500);
      } else {
        if (body.length > 0) {
          var response = body;
        } else {
          var response = {
            "msg" : "Usuario sin cuentas asociadas"
          };
          res.status(404);
        }
      }
        res.send(response);
    }
  )
}


module.exports.getAccountByUserIdV2 = getAccountByUserIdV2;
